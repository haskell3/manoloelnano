package nano;

public abstract class Component {
	protected GameObject gameObject;
	
	public GameObject getGameObject() {
		return gameObject;
	}
	
	public abstract void Update();
	
	public void OnDestroy() {
		gameObject = null;
	}
}
