package nano;

public class EnemigoFactory extends GameObjectPoolableFactory{

	private static EnemigoFactory instance;
	
	private EnemigoFactory() { }
	
	public static EnemigoFactory getInstance() {
		if(instance == null)
			instance = new EnemigoFactory();
		return instance;
	}

	@Override
	public GameObject create() {
		GameObject g = new GameObject();
		g.addComponent(new Enemigos(g));
		g.addComponent(new Vida(g));
		return g;
	}

}
