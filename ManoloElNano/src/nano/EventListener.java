package nano;

import java.util.ArrayList;
import java.util.HashMap;

public class EventListener {

	private static HashMap<String, ArrayList<IObserver>> observers = new HashMap<String, ArrayList<IObserver>>();
	
	public static void register(String event, IObserver o) {
		ArrayList<IObserver> list = observers.getOrDefault(event, new ArrayList<IObserver>());
		list.add(o);
		observers.put(event, list);
	}
	
	public static void unregister(String event, IObserver o) {
		ArrayList<IObserver> list = observers.getOrDefault(event, new ArrayList<IObserver>());
		list.remove(o);
		observers.put(event, list);
	}
	
	public static void notify(String event) {
		for (IObserver o : observers.get(event)) {
			o.raise(event);
		}
	}
}
