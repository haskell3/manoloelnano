package nano;

import java.util.ArrayList;
import java.util.List;

public class GameObject {
	protected List<Component> collection = new ArrayList<Component>();
	protected static int cont;
	protected String id;

	public List<Component> getCollection() {
		return collection;
	}
	 
	public GameObject() {
		cont++;
		id = cont + "";
	}
	
	public void Update() {	}
	
	public void addComponent(Component component) {
		collection.add(component);
	}
	public void removeComponent(Component component) {
		collection.remove(component);
	}
	public boolean hasComponent(Class<? extends Component> component) {
		for (Component c : collection) {
			if( c.getClass().equals(component)) {
				return true;
			}
		}
		return false;
	}
	public Component getComponent(Class<? extends Component> component) {
		for (Component c : collection) {
			if( c.getClass().equals(component)) {
				return c;
			}
		}
		return null;
	}
	
	public void OnDestroy() {
		for (Component component : collection) {
			component.OnDestroy();
		}
		collection.clear();
	}
}
