package nano;

public abstract class GameObjectPoolableFactory implements IPoolableFactory<GameObject>{

	@Override
	public void activate(GameObject element) {
		Nano.dameArgo().addGameObject(element);
	}

	@Override
	public void desactivate(GameObject element) {
		Nano.dameArgo().removeGameObject(element);
	}

	@Override
	public void delete(GameObject element) {
		element.OnDestroy();
		Nano.dameArgo().removeGameObject(element);
	}
}
