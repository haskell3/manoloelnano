package nano;

import java.util.ArrayList;
import java.util.BitSet;

public class GenericPool<T> {

	private ArrayList<T> poolObjects = new ArrayList<T>();
	private BitSet beingUsed;
	private IPoolableFactory<T> m_Factory;
	
	public GenericPool(IPoolableFactory<T> factory, int cant) {
		m_Factory = factory;
		beingUsed = new BitSet(cant);
		for (int i = 0; i < cant; i++) {
			poolObjects.add(m_Factory.create());
		}
	}
	
	public T getElement() {
		
		int in = beingUsed.nextClearBit(0);
		
		if(in > poolObjects.size()) 
			return null;
		
		T object = poolObjects.get(in); 
		beingUsed.set(in, true);
		m_Factory.activate(object);
		return object;
	}
	
	public boolean tryReturnElement(T g) {
		int idx = poolObjects.indexOf(g);
		if((idx==-1) || !beingUsed.get(idx))
			return false;
		beingUsed.set(idx, false);
		m_Factory.desactivate(g);
		return true;
	}
	
	public void Dispose() {
		for (T t : poolObjects) {
			m_Factory.delete(t);
		}
		poolObjects.clear();
		beingUsed.clear();
	}
}
