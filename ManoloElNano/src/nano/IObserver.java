package nano;

public interface IObserver {
	public void raise(String event);	
}
