package nano;

public interface IPoolableFactory<T> {

	public T create();
	public void activate(T element);
	public void desactivate(T element);
	public void delete(T element);
}
