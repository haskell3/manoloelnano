package nano;

import java.util.ArrayList;
import java.util.Random;

public class Nano implements IObserver{

	private static Nano instance;
	private ArrayList<GameObject> objects = new ArrayList<GameObject>();
	private ArrayList<GameObject> objectsToDestroy = new ArrayList<GameObject>();
	private ArrayList<GameObject> newObjects = new ArrayList<GameObject>();
	private long fps;
	private boolean ingame;
	
	private Nano() {
		super();
	}
	
	public double getDeltaTime() {
		return DeltaTime.getDeltaTime();
	}
	
	public static Nano dameArgo() {
		if(instance == null)
			instance = new Nano();
		return instance;
	}
	
	public void corre() {
		
		EventListener.register("ACABAR", this);
		DeltaTime.setDeltaTime();
		ingame = true;
		boolean notGuany = true;
		int guany = new Random().nextInt(8, 15);
		double temps = 0;
		
		System.out.println("Temps a batir: " + guany);
		
		while(ingame) {
			DeltaTime.setDeltaTime();
			temps += DeltaTime.getDeltaTime();
			if(temps >= guany && notGuany) {
				notGuany = false;
				EventListener.notify("CREAR");
			}
			
			update();			
			
			long executionTime = DeltaTime.getExecutionTime();
			if(executionTime < fps) {
				try {
					Thread.sleep(fps - executionTime);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			
			
		}
	}
	
	public void setFps(int fps) {
		this.fps = 1000/fps;
	}
	
	public void update() {
		for (GameObject gameObject : objects) {
			gameObject.Update();
			for (Component component : gameObject.getCollection()) {
				component.Update();
			}
		}
		destroyAndAdd();
	}
	
	private void destroyAndAdd() {
		while(!newObjects.isEmpty()) {
			objects.add(newObjects.remove(0));
		}
		while(!objectsToDestroy.isEmpty()) {
			GameObject g = objectsToDestroy.remove(0);
			if(objects.contains(g))
				objects.remove(g);
			g.OnDestroy();
		}
	}
	
	public void addGameObject(GameObject g) {
		newObjects.add(g);
	}
	
	public void removeGameObject(GameObject g) {
		objectsToDestroy.add(g);
	}

	private class DeltaTime {

		private static double deltaTime;
		private static long lastTime;
		
		public static double getDeltaTime() {
			return deltaTime;
		}
		public static void setDeltaTime() {
			
			long nanos = System.nanoTime();
			
			deltaTime = (nanos - lastTime)/1000000000d;
			
			lastTime = nanos;		
		}	
		
		public static long getExecutionTime() {
			return (System.nanoTime() - lastTime)/1000000;
		}
	}

	@Override
	public void raise(String event) {
		ingame = false;
	}
}


