package nano;

public class ObjecteGuanyadorFactory extends GameObjectPoolableFactory{

	private static ObjecteGuanyadorFactory instance;
	
	private ObjecteGuanyadorFactory() { }
	
	public static ObjecteGuanyadorFactory getInstance() {
		if(instance == null)
			instance = new ObjecteGuanyadorFactory();
		return instance;
	}

	@Override
	public GameObject create() {
		GameObject g = new GameObject();
		g.addComponent(new ObjecteGuanyador());
		return g;
	}
	
	@Override
	public void activate(GameObject element) {
		super.activate(element);
		for (Component c : element.getCollection()) {
			if(c instanceof ObjecteGuanyador)
				((ObjecteGuanyador) c).guanyar();
		}
	}

}
