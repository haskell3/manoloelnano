package nano;

public class Player extends Component implements IObserver{

	private boolean guanyar = false;
	private final String eventGuanyar = "GUANYAR";
	
	public Player(GameObject g) {
		super();
		gameObject = g;
		gameObject.id = "Player";
		EventListener.register(eventGuanyar, this);
	}
	@Override
	public void Update() {	}
	@Override
	public void raise(String event) {
		if(event.equals(eventGuanyar)) {
			if(!guanyar) {
				guanyar = true;
				System.out.println("HE GUANYAAAT!!!");		
				EventListener.notify("ACABAR");
			}
		}
	}
	
	@Override
	public void OnDestroy() {
		super.OnDestroy();
		if(!guanyar)
			System.out.println("He mort abans de poder guanyar");
	}

}
