package nano;

public class PlayerFactory extends GameObjectPoolableFactory{

	private static PlayerFactory instance;
	
	private PlayerFactory() { }
	
	public static PlayerFactory getInstance() {
		if(instance == null)
			instance = new PlayerFactory();
		return instance;
	}

	@Override
	public GameObject create() {
		GameObject g = new GameObject();
		g.addComponent(new Player(g));
		g.addComponent(new Vida(12, g));
		return g;
	}

	
}
