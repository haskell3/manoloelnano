package nano;

public class Spawner extends GameObject implements IObserver{
	
	private final String eventAcabar = "ACABAR";
	private final String eventCrear = "CREAR";
	int cantitat;
	private GenericPool<GameObject> poolE;
	private GenericPool<GameObject> poolP;
	private GenericPool<GameObject> poolO;
	
	public Spawner(int cant) {
		super();
		EventListener.register(eventAcabar, this);
		EventListener.register(eventCrear, this);
		cantitat = cant;
		poolE = new GenericPool<GameObject>(EnemigoFactory.getInstance(), cant);
		poolP = new GenericPool<GameObject>(PlayerFactory.getInstance(), 1);
		poolO = new GenericPool<GameObject>(ObjecteGuanyadorFactory.getInstance(), 1);
		poolP.getElement();
	}
	
	@Override
	public void Update() {
		if(cantitat > 0) {
			cantitat--;
			poolE.getElement();
		}
	}

	@Override
	public void raise(String event) {
		if(event.equals(eventAcabar)) {
			poolE.Dispose();
			poolP.Dispose();
//			poolO.Dispose();
		}else if(event.equals(eventCrear)) {
			poolO.getElement();
		}
	}

}
