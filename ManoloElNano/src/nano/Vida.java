package nano;

import java.util.Random;

public class Vida extends Component {
	public double health;

	private double seconds;
	private double actualSeconds;
	private boolean pooled;
	private GenericPool pool;
	
	public Vida(long hp, GameObject g) {
		health = hp;
		gameObject = g;
		pooled = false;
	}
	
	public Vida(GameObject g) {
		gameObject = g;
		Random r = new Random();
		health = r.nextDouble(1, 10);
		pooled = false;
		System.out.println("Soy " + gameObject.id + " y he nacido con " + (int) health + " PS.");
	}
	
	public Vida(GameObject g, boolean pooled, GenericPool pool) {
		this(g);
		this.pooled = pooled;
		this.pool = pool;
	}
	
	public Vida(long hp, GameObject g, boolean pooled, GenericPool pool) {
		this(hp, g);
		this.pooled = pooled;
		this.pool = pool;
	}
	
	@Override
	public void Update() {
		actualSeconds = Nano.dameArgo().getDeltaTime();
		health -= actualSeconds;
		seconds += actualSeconds;
		if(seconds > 1) {
			System.out.println("Soy " + gameObject.id + " y tengo "+ (int)health + " PS");
			seconds -= 1;
//			System.out.println("Seconds " + seconds);
//			System.out.println("ActualSeconds " + actualSeconds);
//			System.out.println("DeltaTime " + DeltaTime.getDeltaTime());
		}
		if(health <= 0) {
			System.out.println("Soy " + gameObject.id + " y me muero");
			Nano.dameArgo().removeGameObject(this.getGameObject());	
			if(pooled)
				pool.tryReturnElement(gameObject);
		}

	}

	public void OnDestroy() {
		super.OnDestroy();
		pool = null;
	}
}
